package segment;

public class Segment {
	
	private int ext1;
	private int ext2;
	
	public Segment() {}
	
	public Segment( int ext1, int ext2 ) {
		this.ext1 = ext1;
		this.ext2 = ext2;
		ordonne();
	}
	
	public int getExt1() {
		return ext1;
	}
	
	public void setExt1( int ext1 ) {
		this.ext1 = ext1;
		ordonne();
	}
	
	public int getExt2() {
		return ext2;
	}
	
	public void setExt2( int ext2 ) {
		this.ext2 = ext2;
		ordonne();
	}
	
	public void ordonne() {
		if ( ext1 > ext2 ) {
			int tmp = ext1;
			ext1 = ext2;
			ext2 = tmp;
		}
	}
	
	public int getLongueur() {
		return ext2 - ext1;
	}
	
	public boolean appartient( int x ) {
		return x < ext2 && x >= ext1;
	}
}
