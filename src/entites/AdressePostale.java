package entites;

public class AdressePostale {

	private int numeroDeRue;
	private String nomDeRue;
	private int codePostal;
	private String ville;
	
	public AdressePostale() {
		numeroDeRue = 44;
	}
	
	public AdressePostale( int numeroDeRue, String nomDeRue ) {
		this.numeroDeRue = numeroDeRue;
		this.nomDeRue = nomDeRue;
	}
	
	public AdressePostale( int numeroDeRue, String nomDeRue, int codePostal, String ville ) {
		this.numeroDeRue = numeroDeRue;
		this.nomDeRue = nomDeRue;
		this.codePostal = codePostal;
		this.ville = ville;
	}
	
	public int getNumeroDeRue() {
		return numeroDeRue;
	}
	
	public void setNumeroDeRue( int numeroDeRue ) {
		this.numeroDeRue = numeroDeRue;
	}
	
	public String getNomDeRue() {
		return nomDeRue;
	}
	
	public void setNomDeRue( String nomDeRue ) {
		this.nomDeRue = nomDeRue;
	}
	
	public int getCodePostal() {
		return codePostal;
	}
	
	public void setCodePostal( int codePostal ) {
		this.codePostal = codePostal;
	}
	
	public String getVille() {
		return ville;
	}
	
	public void setVille( String ville ) {
		this.ville = ville;
	}
}
