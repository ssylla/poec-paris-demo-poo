package essais;

import entites.AdressePostale;
import entites.Personne;
import entites.SalleDeSport;

public class TestPersonne {
	
	public static void main( String[] args ) {
		
		int a = 20;
		
		Personne personne = new Personne();
		
		personne.setNom( "sylla");
		System.out.println(personne.getNom());
		personne.setPrenom( "S" );
		personne.setAdressePostale( new AdressePostale(15, "BD FOCH", 49000, "Angers") );
		personne.getAdressePostale().setNumeroDeRue( 15 );
		personne.getAdressePostale().setNomDeRue( "BD FOCH" );
		
		Personne personne1 = new Personne();
		personne1.setNom( "TAPA" );
		personne1.setAdressePostale( personne.getAdressePostale() );
		System.out.println(personne.getAdressePostale().getNumeroDeRue());
		System.out.println(personne.getAdressePostale().getNomDeRue());
		
		SalleDeSport sds = new SalleDeSport();
		sds.setAdressePostale( personne1.getAdressePostale() );
	}
}
