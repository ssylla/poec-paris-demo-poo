package essais;

import segment.Segment;

import java.util.Scanner;

public class TestSegment {
	
	public static void main( String[] args ) {
		
		Scanner sc = new Scanner( System.in );
		System.out.println( "Gestion des segments" );
		System.out.print( "Entrez la 1ere extrémité : " );
		int ext1 = sc.nextInt();
		sc.nextLine();
		System.out.print( "Entrez la 2e extrémité : " );
		int ext2 = sc.nextInt();
		sc.nextLine();
		
		Segment segment = new Segment( ext1, ext2 );
		int response;
		
		do {
			System.out.println( "Que souhaitez-vous faire ?" );
			System.out.println( "1 : Calculer la longueur" );
			System.out.println( "2 : Verifier une appartenance" );
			System.out.println( "3 : Modifier l'extrémité 1" );
			System.out.println( "4 : Modifier l'extrémité 2" );
			System.out.println( "5 : Afficher le segment" );
			System.out.println( "6 : Quitter" );
			System.out.print( "Quel est votre choix : " );
			response = sc.nextInt();
			sc.nextLine();
			
			switch ( response ) {
				case 1:
					System.out.println( "La longueur du segment est : " + segment.getLongueur() );
					break;
				case 2:
					System.out.print( "Entrez le point à tester : " );
					int x = sc.nextInt();
					sc.nextLine();
					if ( segment.appartient( x ) ) {
						System.out.println( x + " appartient au segment" );
					} else {
						System.out.println( x + " n'appartient pas au segment" );
					}
					break;
			}
		} while ( response > 0 && response < 6 );
	}
}
