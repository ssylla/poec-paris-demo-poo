package entites;

public class SalleDeSport {
	
	private String nom;
	private AdressePostale adressePostale;
	private Personne[] adherants;
	
	
	public SalleDeSport() {
	}
	
	
	public String getNom() {
		return nom;
	}
	
	public void setNom( String nom ) {
		this.nom = nom;
	}
	
	public AdressePostale getAdressePostale() {
		return adressePostale;
	}
	
	public void setAdressePostale( AdressePostale adressePostale ) {
		this.adressePostale = adressePostale;
	}
}
