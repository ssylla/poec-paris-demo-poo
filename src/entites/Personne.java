package entites;

import java.util.Locale;

public class Personne {
	
	private String nom;
	private String prenom;
	private AdressePostale adressePostale;
	
	public Personne() {
	}
	
	public Personne( String nom, String prenom, AdressePostale adressePostale ) {
		this.nom = nom;
		this.prenom = prenom;
		this.adressePostale = adressePostale;
	}
	
	public String getNom() {
		return nom;
	}
	
	public void setNom( String nom ) {
		this.nom = nom.toUpperCase( Locale.ROOT );
	}
	
	public String getPrenom() {
		return prenom;
	}
	
	public void setPrenom( String prenom ) {
		this.prenom = prenom;
	}
	
	public AdressePostale getAdressePostale() {
		return adressePostale;
	}
	
	public void setAdressePostale( AdressePostale adressePostale ) {
		this.adressePostale = adressePostale;
	}
}
